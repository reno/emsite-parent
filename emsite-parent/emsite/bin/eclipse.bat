@echo off
rem /**
rem  * Copyright &copy; 2017-2018 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
rem  *
rem  * Author: haotking@163.com
rem  */
echo.
echo [信息] 生成Eclipse工程文件。
echo.
pause
echo.

cd /d %~dp0
cd..

call mvn -Declipse.workspace=%cd% eclipse:clean eclipse:eclipse

cd bin
pause