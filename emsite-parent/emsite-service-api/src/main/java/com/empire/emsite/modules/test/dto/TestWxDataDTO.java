/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.test.dto;

import com.empire.emsite.common.dto.BaseDataDTO;

/**
 * 类TestWxDataDTO.java的实现描述：testwxDTO
 * 
 * @author arron 2018年2月24日 下午4:38:41
 */
public class TestWxDataDTO extends BaseDataDTO {

    private String openId; // open_id
    private String name;  // 昵称

    public TestWxDataDTO() {
        super();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
